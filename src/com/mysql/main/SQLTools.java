package com.mysql.main;

import java.sql.*;

import net.ansraer.core.AnsraerCore;

public class SQLTools {
	
	public static boolean valueExists(String table, String datenabfrage, String identifyKey){
		
		try {
			ResultSet rs = AnsraerCore.mysql.query("SELECT * FROM "+table+" WHERE "+datenabfrage+"= '" + identifyKey + "'");
			
			if(rs.next()){
				return rs.getString(datenabfrage) != null;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	//Creating basic player data on first join??
	public static void createValue(String table, String datenabfrage, String identifyKey) {
		//Creates first entry into database if player is not in database yet.
		if(!(valueExists(table, datenabfrage, identifyKey))){
				AnsraerCore.mysql.update("INSERT INTO "+table+"("+ datenabfrage +") VALUES ('" + identifyKey + "');");

		}
		
	}
	
	
	/**
	 * Returns an Boolean
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column Column from where you want the value.
	 * @param identifyKey Used to identify row.
	 * @return
	 */
	public static Integer getBoolean(String table, String datenabfrage, String column, String identifyKey){
		
		Integer value = null;
		if(valueExists(table, datenabfrage, identifyKey)){
			
			try {
				ResultSet rs = AnsraerCore.mysql.query("SELECT * FROM "+table+" WHERE "+datenabfrage+"= '" + identifyKey + "'");
				value = rs.getInt(column);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else{
			createValue(table, datenabfrage, identifyKey);
			getBoolean(table, datenabfrage, column, identifyKey);
		}
		
		return value;
		
	}
	
	/**
	 * Sets an Boolean in specified table and row.
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column Column
	 * @param identifyKey Used to identify row.
	 */
	public static void setBoolean(String table, String datenabfrage, String column, String identifyKey, Integer value){
		
		if(valueExists(table, datenabfrage, identifyKey)){
			AnsraerCore.mysql.update("UPDATE "+table+" SET "+column+"= '" + value + "' WHERE "+datenabfrage+"= '" + identifyKey + "';");
			
		} else{
			createValue(table, datenabfrage, identifyKey);
			setInt(table, datenabfrage, column, identifyKey, value);
		}
		
	}
	
	
	
	/**
	 * Returns an Integer
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column Column from where you want the value.
	 * @param identifyKey Used to identify row.
	 * @return
	 */
	public static Integer getInt(String table, String datenabfrage, String column, String identifyKey){
		
		Integer value = null;
		if(valueExists(table, datenabfrage, identifyKey)){
			
			try {
				ResultSet rs = AnsraerCore.mysql.query("SELECT * FROM "+table+" WHERE "+datenabfrage+"= '" + identifyKey + "'");
				if((!rs.next()) || (Integer.valueOf(rs.getInt(column)) == null));
				value = rs.getInt(column);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else{
			createValue(table, datenabfrage, identifyKey);
			getInt(table, datenabfrage, column, identifyKey);
		}
		
		return value;
		
	}
	
	/**
	 * Sets an int in specified table and row.
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column Column
	 * @param identifyKey Used to identify row.
	 */
	public static void setInt(String table, String datenabfrage, String column, String identifyKey, Integer value){
		
		if(valueExists(table, datenabfrage, identifyKey)){
			AnsraerCore.mysql.update("UPDATE "+table+" SET "+column+"= '" + value + "' WHERE "+datenabfrage+"= '" + identifyKey + "';");
			
		} else{
			createValue(table, datenabfrage, identifyKey);
			setInt(table, datenabfrage, column, identifyKey, value);
		}
		
	}
	
	/**
	 * Adds an int to an already existing int.
	 * @param datenabfrage Used to identify row.
	 * @param column Column
	 * @param identifyKey Used to identify row.
	 */
	public static void addInt(String table, String datenabfrage, String column, String identifyKey, Integer value){
		
		if(valueExists(table, datenabfrage, identifyKey)){
			AnsraerCore.mysql.update("UPDATE "+table+" SET "+column+"= '" + (value+ SQLTools.getInt(table, datenabfrage, column, identifyKey)) + "' WHERE "+datenabfrage+"= '" + identifyKey + "';");
		} else{
			createValue(table, datenabfrage, identifyKey);
			addInt(table, datenabfrage, column, identifyKey, value);
		}
		
	}
	
	/**
	 * Gets a String from specified table.
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column	Column
	 * @param identifyKey Used to identify row.
	 * @return
	 */
	public static String getString(String table, String datenabfrage, String column, String identifyKey){
		
		String value = "";

		if(valueExists(table, datenabfrage, identifyKey)){
			
			try {
				ResultSet rs = AnsraerCore.mysql.query("SELECT * FROM "+table+" WHERE "+datenabfrage+"= '" + identifyKey + "'");
				if((!rs.next()) || (rs.getString(column) == null));
				
				value = rs.getString(column);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else{
			createValue(table, datenabfrage, identifyKey);
			getString(table, datenabfrage, column, identifyKey);
		}
		
		
		return value;
		
	}
	
	/**Sets an String in specified table and row.
	 * 
	 * @param datenabfrage Used to identify row.
	 * @param column Column
	 * @param identifyKey Used to identify row.
	 */
	public static void setString(String table, String datenabfrage, String column, String identifyKey, String value){		
		if(valueExists(table, datenabfrage, identifyKey)){
			System.out.println("Value exists, now adding String");
			AnsraerCore.mysql.update("UPDATE "+table+" SET "+column+"= '" + value + "' WHERE "+datenabfrage+"= '" + identifyKey + "';");
			
		} else{
			System.out.println("Setting string failed, now creating Value");
			createValue(table, datenabfrage, identifyKey);
			setString(table, datenabfrage, column, identifyKey, value);
		}
		
	}
	
	
	
	
}
