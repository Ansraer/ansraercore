package net.ansraer.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.mysql.main.MySQL;

import net.ansraer.core.ranks.Ranks;
import net.ansraer.core.ranks.events.PermissionEvents;


public class AnsraerCore extends JavaPlugin{
	
	public static AnsraerCore plugin;
	public static Logger log;
	
	public static MySQL mysql;
	public static List<String> tableAutoAddPlayers = new ArrayList<String>();

	//used for permissions
	public static HashMap<UUID,PermissionAttachment> playerPermissions = new HashMap<UUID, PermissionAttachment>();
	public static HashMap<String,Integer> playerRankID = new HashMap<String, Integer>();
	
	
	public void onEnable(){
		
		plugin = this;
		log = this.getLogger();
		
		System.out.println("[AnsraerCore] Plugin wurde gestartet");
		ConnectMySQL();
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new PermissionEvents(), this);
		
		
		Ranks.update();
	}
	
	
	public void onDisable(){
		
		//removes all permissions
		for(Player player : getServer().getOnlinePlayers()){
			player.removeAttachment(playerPermissions.get(player.getUniqueId()));
		}
		
		System.out.println("[AnsraerCore] Plugin wurde gestoppt");
		
	}
	
	//new MySQL("ni343346_1.vweb06.nitrado.net", "ni343346_1sql7", "ni343346_1sql7", "8097786f");
	private void ConnectMySQL(){
		mysql = new MySQL("ni343346_1.vweb06.nitrado.net", "ni343346_1sql7", "ni343346_1sql7", "8097786f");
		mysql.update("CREATE TABLE IF NOT EXISTS PlayerData(UUID varchar(64), P_NAME varchar(64), RANK_ID int);");
		mysql.update("CREATE TABLE IF NOT EXISTS Ranks(RANK_ID int, RANK_NAME varchar(64), RANK_POWER int, RANK_NAMEFORMAT varchar(64), RANK_CHATFORMAT varchar(64), RANK_BELOW_NAME boolean);");
	}


	
	/**
	 * Table that require the players UUID will have it added as soon as the player joins. 
	 * WARNING! Table must have an 'UUID' column!
	 * 
	 * @param tableName Name of the already created Table
	 */
	public void addAutoAddPlayersTable(String tableName){
		tableAutoAddPlayers.add(tableName);
	}
	

	
}
