package net.ansraer.core.ranks;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import com.mysql.main.SQLTools;

import net.ansraer.core.AnsraerCore;

public class Ranks {
	public static HashMap<Integer,Rank> ranks = new HashMap<Integer, Rank>();

	
	public static void update() {

		try {
			ResultSet rs = AnsraerCore.mysql.query("SELECT * FROM Ranks");
			while (rs.next()) {
				
				 int id = rs.getInt("RANK_ID");
				 String name = rs.getString("RANK_NAME");
				 int power = rs.getInt("RANK_POWER");
				 String nameformat = rs.getString("RANK_NAMEFORMAT");
				 String chatformat = rs.getString("RANK_CHATFORMAT");
				 boolean rankBelowName = rs.getBoolean("RANK_BELOW_NAME");
				 
				 Rank rank = new Rank(id,name,power,nameformat,chatformat, rankBelowName);
				 
				 ranks.put(id, rank);
				 
				 
			}
		} catch (SQLException e) {
			AnsraerCore.log.warning("Problem occured during download of ranks.");
			e.printStackTrace();
		}
	}
	
	
	public static String getRankNameFromRankID(int id){
		if(ranks.get(id) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", id+"", "UNNAMED");
			update();
		}	
		return ranks.get(id).name;
	}
	
	public static int getPowerLevelFromRankID(int id){
		if(ranks.get(id) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", id+"", "UNNAMED");
			update();
		}
		return ranks.get(id).powerLevel;
	}
	
	public static String getNameFormatFromRankID(int id){
		if(ranks.get(id) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", id+"", "UNNAMED");
			update();
		}
		if(ranks.get(id).nameformat == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAMEFORMAT", id+"", "");
			update();
		}
		return ranks.get(id).nameformat;
	}
	
	public static String getChatFormatFromRankID(int id){
		if(ranks.get(id) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", id+"", "UNNAMED");
			update();
		}
		if(ranks.get(id).chatformat == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_CHATFORMAT", id+"", "");
			update();
		}
		return ranks.get(id).chatformat;
	}
	
	public static boolean getBelowNameFromID(int id){
		if(ranks.get(id) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", id+"", "UNNAMED");
			update();
		}

		return ranks.get(id).rankBelowName;
	}
	
	
	
	
	
	public static int getRankIDFromUUID(UUID uuid){
		return AnsraerCore.playerRankID.get(uuid.toString());
	}
	
	public static String getRankNameFromUUID(UUID uuid){
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", AnsraerCore.playerRankID.get(uuid.toString())+"", "UNNAMED");
			update();
		}
		return ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).name;
	}
	
	public static int getPowerLevelFromUUID(UUID uuid){
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", AnsraerCore.playerRankID.get(uuid.toString())+"", "UNNAMED");
			update();
		}
		return ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).powerLevel;
	}
	
	public static String getNameFormatFromUUID(UUID uuid){
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", AnsraerCore.playerRankID.get(uuid.toString())+"", "UNNAMED");
			update();
		}
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).nameformat == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAMEFORMAT", AnsraerCore.playerRankID.get(uuid.toString())+"", "");
			update();
		}
		return ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).nameformat;
	}
	
	public static String getChatFormatFromUUID(UUID uuid){
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())) == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", AnsraerCore.playerRankID.get(uuid.toString())+"", "UNNAMED");
			update();
		}
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).chatformat == null){
			SQLTools.setString("Ranks", "RANK_ID", "RANK_CHATFORMAT", AnsraerCore.playerRankID.get(uuid.toString())+"", "");
			update();
		}
		return ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).chatformat;
	}
	
	public static boolean getBelowNameFromUUID(UUID uuid){
		if(ranks.get(AnsraerCore.playerRankID.get(uuid.toString())) == null){
				SQLTools.setString("Ranks", "RANK_ID", "RANK_NAME", AnsraerCore.playerRankID.get(uuid.toString())+"", "UNNAMED");
				update();
		}

		return ranks.get(AnsraerCore.playerRankID.get(uuid.toString())).rankBelowName;
	}

}
