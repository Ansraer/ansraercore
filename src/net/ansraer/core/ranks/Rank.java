package net.ansraer.core.ranks;

public class Rank {

	public int id;
	public String name;
	public int powerLevel;
	String nameformat;
	String chatformat;
	boolean rankBelowName;
	
	public Rank(int id, String name, int power, String nameformat, String chatformat, boolean rankBelowName) {
		this.id=id;
		this.name=name;
		this.powerLevel = power;
		this.nameformat=nameformat;
		this.chatformat=chatformat;
		this.rankBelowName = rankBelowName;
	}
}
