package net.ansraer.core.ranks.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

import com.mysql.main.SQLTools;

import net.ansraer.core.AnsraerCore;
import net.ansraer.core.ranks.Ranks;

public class PermissionEvents implements Listener{
	
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e){
		Player player = e.getPlayer();
		
		String uuid = e.getPlayer().getUniqueId().toString();
		String pname = e.getPlayer().getName();
		

		//updates player name and adds player to the autoAddtable
		SQLTools.setString("PlayerData", "UUID", "P_NAME", uuid, pname);
		for(String table : AnsraerCore.tableAutoAddPlayers){
			SQLTools.createValue(table, "UUID", uuid);
		}
		
		//Makes all new players to rank 0
		if(!SQLTools.valueExists("PlayerData", "RANK_ID", uuid)){
			SQLTools.setInt("PlayerData", "UUID", "RANK_ID", uuid, 0);
		}
		
		//gets rankID and adds them to the players.
		AnsraerCore.playerRankID.put(player.getUniqueId().toString(), SQLTools.getInt("PlayerData", "UUID", "RANK_ID", uuid));
		
		PermissionAttachment permissionAttachment = player.addAttachment(AnsraerCore.plugin);
		permissionAttachment.setPermission(Ranks.getRankNameFromUUID(player.getUniqueId()), true);
		AnsraerCore.playerPermissions.put(player.getUniqueId(), permissionAttachment);
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e){
		e.getPlayer().removeAttachment(AnsraerCore.playerPermissions.get(e.getPlayer().getUniqueId()));
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		e.getPlayer().removeAttachment(AnsraerCore.playerPermissions.get(e.getPlayer().getUniqueId()));
	}

}
